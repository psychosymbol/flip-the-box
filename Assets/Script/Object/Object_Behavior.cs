﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SpriteRenderer))]
public class Object_Behavior : MonoBehaviour {
    
    public bool _stopMoving = true;
    private List<GameObject> _pairColor = new List<GameObject>();
    public bool _inGoal;
    public bool _completeGoal;
    public float _objectSpeed;

    public bool _canMove;
    public Color _objectColor;

    private Vector2 _targetPosition;
    Stage_Manager _stageManager;

    private void Start()
    {
        _stageManager = Stage_Manager.instance;

        _targetPosition = this.transform.position;

        _objectColor = this.GetComponent<SpriteRenderer>().color;

        GameObject[] pairFinder = GameObject.FindGameObjectsWithTag("Object");

        for (int i = 0; i < pairFinder.Length; i++)
        {
            if (pairFinder[i] != this.gameObject && pairFinder[i].GetComponent<SpriteRenderer>().color == this.GetComponent<SpriteRenderer>().color)
                _pairColor.Add(pairFinder[i]);
        }

    }

    private void Update()
    {
        if (_stageManager._letObjectMove)
        {
            if (_stopMoving)
            {
                _targetPosition = getTargetPosition(_stageManager.GetDownWardDirection(), this.transform.position);
                _stageManager.GetDownWardDirection();
                _stopMoving = false;
            }
            _stageManager._rotating = true;
            this.transform.position = Vector2.MoveTowards(this.transform.position, _targetPosition, Time.deltaTime * _objectSpeed);
        }

        if ((Vector2)this.transform.position == _targetPosition && !_stopMoving)
        {
            _stageManager._rotating = false;
            _stageManager._letObjectMove = false;
            _stopMoving = true;

            //Check for goal here as this part is only cal for one frame
            try
            {
                if(GameObject.Find(this.transform.position.x + " x " + this.transform.position.y).CompareTag("Goal"))
                {
                    Debug.Log("GOAL");
                    Game_Manager.instance.currentState.toNextState(0);
                }
            }
            catch (System.Exception)
            {
                Debug.Log("There's no grid at " + this.transform.position.x + " x " + this.transform.position.y + ". Is the object stay in grid?");
            }
        }
    }

    Vector2 getTargetPosition(Static_Direction.direction direction, Vector2 currentPosition)
    {
        Vector2 nextPosition = currentPosition;
        Stage_SolutionChecker stageSolutionChecker = Stage_SolutionChecker.instance;

        while (!stageSolutionChecker.Check(nextPosition, direction)) //SOMEHOW THIS END UP AS NULLREFฯ EXCEPTION //Because Stage_SolutionChecker isn't in the scene at the time this was call //CLEAR
        {
            nextPosition = Static_Direction.moveTowardDirection(nextPosition, direction); // Somehow this loop stuck //Because the direction that's return from Stage_Manager is NONE //CLEAR
        }

        return nextPosition;
    }
}
