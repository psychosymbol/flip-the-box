﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Static_Direction : MonoBehaviour {

    //this class is to defy the direction in the game to do things like checking answer or generate the puzzle or even check for the direction the object have to move

    public enum direction
    {
        RIGHT = 0,
        UP = 1,
        LEFT = 2,
        DOWN = 3,
        NONE = 999
    }

    public static List<direction> switchDirectionXY(direction direction)
    {
        List<direction> directions = new List<direction>();
        switch (direction)
        {
            case direction.RIGHT:
            case direction.LEFT:
                directions.Add(direction.UP);
                directions.Add(direction.DOWN);
                break;
            case direction.DOWN:
            case direction.UP:
                directions.Add(direction.LEFT);
                directions.Add(direction.RIGHT);
                break;
        }
        return directions;
    }

    public static direction inverseDirection(direction direction)
    {
        direction newDirection = direction.NONE;
        switch (direction)
        {
            case direction.RIGHT:
                newDirection = direction.LEFT;
                break;
            case direction.UP:
                newDirection = direction.DOWN;
                break;
            case direction.LEFT:
                newDirection = direction.RIGHT;
                break;
            case direction.DOWN:
                newDirection = direction.UP;
                break;
        }

        return newDirection;
    }

    public static Vector2 moveTowardDirection(Vector2 position, Static_Direction.direction direction)
    {
        Vector2 nextPosition = position;
        switch (direction)
        {
            case Static_Direction.direction.RIGHT:
                nextPosition.x += Grid_Generator.instance._spriteSize;
                break;
            case Static_Direction.direction.DOWN:
                nextPosition.y -= Grid_Generator.instance._spriteSize;
                break;
            case Static_Direction.direction.LEFT:
                nextPosition.x -= Grid_Generator.instance._spriteSize;
                break;
            case Static_Direction.direction.UP:
                nextPosition.y += Grid_Generator.instance._spriteSize;
                break;
        }
        return nextPosition;
    }

}
