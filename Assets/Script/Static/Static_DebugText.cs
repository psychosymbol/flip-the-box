﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Static_DebugText : MonoBehaviour {

    //this class is for logging the action to debug what you did in the code through the panel in game to make it easier to check

    public static Static_DebugText instance;
    public TextMeshProUGUI texts;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        texts = gameObject.GetComponent<TextMeshProUGUI>();
    }
    public void Debug(string value)
    {
        texts.text = value;
    }
    public void DebugAdd(string value)
    {
        texts.text += value;
    }
}
