﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class Game_Manager : MonoBehaviour
{

    public static Game_Manager instance;
    public Game_State currentState;
    public List<Game_State> _states = new List<Game_State>();
    public string _startState;

    [HideInInspector]
    public string _fileName, _puzzleID;


    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

        for (int i = 0; i < this.gameObject.transform.childCount; i++)
        {
            if (this.gameObject.transform.GetChild(i).gameObject.GetComponent<Game_State>() != null)
                _states.Add(this.gameObject.transform.GetChild(i).gameObject.GetComponent<Game_State>());
        }

        ChangeState(_startState);
    }

    public void ChangeState(string state)
    {
        if (currentState != null)
            currentState.transOut();

        for (int i = 0; i < _states.Count; i++)
        {
            if (_states[i].getName() == state)
            {
                currentState = _states[i];
                currentState.transIn();
                break;
            }
        }
    }


    
}
