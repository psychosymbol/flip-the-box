﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_State : MonoBehaviour
{
    protected Game_Manager game_Manager;

    public List<string> _nextState = new List<string>();

    public virtual void transIn()
    {
        //initialized state
        game_Manager = Game_Manager.instance;

        this.gameObject.SetActive(true);
    }

    public virtual void transOut()
    {
        //exit state

        //for debug purpose
        this.gameObject.SetActive(false);
        //game_Manager.ChangeState("");
    }

    public virtual string getName()
    {
        string name = this.gameObject.name;

        string[] names = name.Split('_');

        name = names[names.Length - 1];

        return name;
    }

    public virtual void toNextState(int value)
    {
        game_Manager.ChangeState(_nextState[value]);
    }
}
