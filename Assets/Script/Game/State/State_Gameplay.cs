﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class State_Gameplay : Game_State {
    public Text _playerMove, _intendMove;
    public override void transIn()
    {
        base.transIn();
    }

    public override void transOut()
    {
        base.transOut();
    }

    public override string getName()
    {
        return base.getName();
    }

    private void Update()
    {
        _playerMove.text = "Player Move : " + Stage_Manager.instance._move;
        _intendMove.text = "Move Needed : " + Grid_Generator.instance._maxStepForLevel;
    }

    public override void toNextState(int value)
    {
        GameObject.FindGameObjectWithTag("MainCamera").transform.localEulerAngles = Vector3.zero; //reset camera for the next use
        base.toNextState(value);
    }
}
