﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class State_DifficultySelect : Game_State {

    public GameObject _ButtonUISet;

    public override void transIn()
    {
        base.transIn();
        getFileFromFolder();
    }

    public override void transOut()
    {
        base.transOut();
    }

    public override string getName()
    {
        return base.getName();
    }

    public override void toNextState(int value)
    {
        //game_Manager.ChangeState(_nextState[value]);
        base.toNextState(value);
    }

    public void getFileFromFolder()
    {
        string path = Application.dataPath + "/PuzzleSave";
        var info = new DirectoryInfo(path);
        var fileInfo = info.GetFiles("*.json");

        int itemNum = 0;
        int buttonNum = _ButtonUISet.transform.childCount;
        //Add function for every button that're intend to be used and enabled them
        foreach (var file in fileInfo)
        {
            string fileName = file.Name;
            string[] name = fileName.Split('.');

            GameObject buttonUI = _ButtonUISet.transform.GetChild(itemNum).gameObject;
            buttonUI.SetActive(true);

            Text buttonText = buttonUI.transform.GetChild(0).gameObject.GetComponent<Text>();
            buttonText.text = name[0];

            Button button = buttonUI.GetComponent<Button>();
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(delegate { returnString(fileName); });

            itemNum++;
        }
        //Disable any button left and remove all function in them too
        for (int i = itemNum; i < buttonNum; i++)
        {
            GameObject buttonUI = _ButtonUISet.transform.GetChild(i).gameObject;

            Button button = buttonUI.GetComponent<Button>();
            button.onClick.RemoveAllListeners();

            buttonUI.SetActive(false);
        }

    }

    void returnString(string value)
    {
        game_Manager._fileName = value;
        game_Manager.ChangeState(_nextState[0]);
    }
}
