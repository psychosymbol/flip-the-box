﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class State_PuzzleSelect : Game_State {
    
    public GameObject _ButtonUISet;
    string _filePath;

    public override void transIn()
    {
        base.transIn();
        getSaveFromFile();
    }

    public override void transOut()
    {
        base.transOut();
    }

    public override string getName()
    {
        return base.getName();
    }

    public override void toNextState(int value)
    {
        base.toNextState(value);
    }

    public void getSaveFromFile()
    {
        _filePath = Application.dataPath + "/PuzzleSave/" + game_Manager._fileName;
        Save_SaveData save = JsonUtility.FromJson<Save_SaveData>(File.ReadAllText(_filePath));

        int itemNum = 0;
        int buttonNum = _ButtonUISet.transform.childCount;
        //Add function for every button that're intend to be used and enabled them
        //** Add function for every button that when they're clicked they will save the puzzle id to Game Manager for retry sake too.
        foreach (Save_SaveStruct saves in save.Saves)
        {
            string name = save.Saves[itemNum].ID;

            GameObject buttonUI = _ButtonUISet.transform.GetChild(itemNum).gameObject;
            buttonUI.SetActive(true);
            
            Text buttonText = buttonUI.transform.GetChild(0).gameObject.GetComponent<Text>();
            buttonText.text = name;

            Button button = buttonUI.GetComponent<Button>();
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(delegate { LoadPuzzle(name); });

            itemNum++;
        }
        //Disable any button left and remove all function in them too
        for (int i = itemNum; i < buttonNum; i++)
        {
            GameObject buttonUI = _ButtonUISet.transform.GetChild(i).gameObject;

            Button button = buttonUI.GetComponent<Button>();
            button.onClick.RemoveAllListeners();

            buttonUI.SetActive(false);
        }
    }

    //Change to Load Puzzle
    void LoadPuzzle(string value)
    {
        Save_Manager.instance.LoadPuzzle(_filePath, value);
        game_Manager._puzzleID = value; 
        Grid_Generator.instance.GenGridFormSave();
        game_Manager.ChangeState(_nextState[0]);
    }
}
