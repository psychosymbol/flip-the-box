﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State_GameEnd : Game_State {

    public Transform _starGroup;

    public override void transIn()
    {
        //Cal Star rate
        int starRate = 3 + (Grid_Generator.instance._maxStepForLevel - Stage_Manager.instance._move);

        for (int i = 0; i < _starGroup.childCount; i++)
        {
            if (i < starRate)
                _starGroup.GetChild(i).gameObject.SetActive(true);
            else
                _starGroup.GetChild(i).gameObject.SetActive(false);
        }

        Stage_Manager.instance._move = 0; //reset move parameter after use it to calculate star rate

        base.transIn();
    }

    public override void transOut()
    {
        base.transOut();
    }

    public override string getName()
    {
        return base.getName();
    }

    public override void toNextState(int value)
    {
        base.toNextState(value);
    }

    public void Retry()
    {
        string path = Application.dataPath + "/PuzzleSave/" + game_Manager._fileName;
        Save_Manager.instance.LoadPuzzle(path, game_Manager._puzzleID);
        Grid_Generator.instance.GenGridFormSave();
    }

}
