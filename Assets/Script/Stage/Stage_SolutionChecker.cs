﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Stage_SolutionChecker : MonoBehaviour
{
    public static Stage_SolutionChecker instance;
    public GameObject _pointer;

    struct Move
    {
        public Vector2 Grid;
        public Static_Direction.direction Direction;
    }
    

    //to show Pointer
    struct solutionGuidance
    {
        public List<GameObject> pointerList;
        public int solutionStep;
    }

    //to simulated Answer
    struct solutionSimulator
    {
        public List<Static_Direction.direction> directions;
        public int solutionStep;
    }

    [SerializeField]
    int _maximumStep;

    int _possibleSolution;
    int _fewestStep;
    
    Dictionary<Move, int> _usedMoves = new Dictionary<Move, int>();
    solutionGuidance _solutions = new solutionGuidance();
    solutionSimulator _simulator = new solutionSimulator();

    public bool test;
    

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    public void doCheck()
    {
        _usedMoves.Clear();
        _fewestStep = _maximumStep;
        _solutions.solutionStep = _fewestStep;

        GameObject[] objectList = GameObject.FindGameObjectsWithTag("Object");

        Debug.Log("Start Check");

        for (int i = 0; i < objectList.Length; i++)
        {
            GameObject objectIden = objectList[i];

            for (int j = 0; j < 4; j++)
            {
                if (Check(objectIden.transform.position, (Static_Direction.direction)j))
                {
                    Move currentMove = new Move();
                    currentMove.Grid = objectIden.transform.position;
                    currentMove.Direction = (Static_Direction.direction)j;

                    try
                    {
                        int x = _usedMoves[currentMove];
                    }
                    catch (System.Exception)
                    {
                        _usedMoves.Add(currentMove, 0);
                    }
                }
                else
                {
                    Color setColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);

                    solutionSimulator newSimulator = new solutionSimulator();
                    newSimulator.solutionStep = 0;
                    newSimulator.directions = new List<Static_Direction.direction>();
                    newSimulator.directions.Add((Static_Direction.direction)j);

                    StartCoroutine(Check(objectIden.transform.position, 1, (Static_Direction.direction)j, setColor, new List<GameObject>(), newSimulator));
                }
            }

        }
        Static_DebugText.instance.Debug("Checking for the solution. When finished, you can click simulated to see the answer");
    }
    
    IEnumerator Check(Vector2 currentPosition, int currentStep, Static_Direction.direction direction, Color color, List<GameObject> pointerList, solutionSimulator simulators)
    {
        Debug.Log("Enum Check Step : " + currentStep);
        if (currentStep <= _fewestStep)
        {
            Vector2 nextPosition = Static_Direction.moveTowardDirection(currentPosition, direction);
            Debug.Log("Pass the step");
            if (GameObject.Find(nextPosition.x + " x " + nextPosition.y) != null)
            {
                GameObject target = GameObject.Find(nextPosition.x + " x " + nextPosition.y);

                if (target.tag.Contains("Goal")) //Check if the next grid is Goal
                {
                    if (Check(nextPosition, direction)) //Check if there's any wall to break the object so it can land on the Goal
                    {
                        _possibleSolution++;
                        _fewestStep = currentStep;
                        if (currentStep <= _solutions.solutionStep)
                        {
                            clearGameObjectList(_solutions.pointerList);
                            
                            _solutions.pointerList = pointerList;
                            _solutions.solutionStep = currentStep;
                            
                            simulators.solutionStep = _solutions.solutionStep;
                            finalizedSolution(simulators);
                        }
                        else
                        {
                            clearGameObjectList(pointerList);
                        }
                    }
                    else
                    {
                        //if not, moving on to check for the next grid
                        StartCoroutine(Check(nextPosition, currentStep, direction, color, pointerList, simulators));
                    }
                }
                else if (target.tag.Contains("Grid")) //Check if the next grid is just a normal Grid
                {
                    //Mark the moved Position
                    SpawnPointer(nextPosition, direction, color, pointerList);
                    //Moving on to check for the next grid
                    StartCoroutine(Check(nextPosition, currentStep, direction, color, pointerList, simulators));
                }
                else if (target.tag.Contains("Wall")) //Check if the next grid is Wall
                {
                    //this variable is for checking if the current move is already checked by other loop
                    bool gridIsUsed = false;

                    Move currentMove = new Move();
                    currentMove.Grid = currentPosition;
                    currentMove.Direction = direction;

                    try
                    {
                        if (currentStep > _usedMoves[currentMove]) //Check if the grid we currently checking is already been check before to cancel any stupid deadlock loop attemping to check for the same useless move forever
                        {
                            gridIsUsed = true;
                        }
                    }
                    catch (System.Exception)
                    {
                    }

                    if (!gridIsUsed)
                    {
                        try
                        {
                            if(_usedMoves[currentMove] > currentStep)
                            {
                                _usedMoves.Remove(currentMove);
                                _usedMoves.Add(currentMove, currentStep);
                            }
                        }
                        catch (System.Exception)
                        {
                            _usedMoves.Add(currentMove, currentStep);
                        }

                        //Rotate to next direction
                        int lastDirection = (int)Static_Direction.inverseDirection(direction);
                        
                        for (int j = 0; j < 4; j++)
                        {
                            if (j != (int)direction && j != lastDirection && !Check(currentPosition, (Static_Direction.direction)j)) //Check if the new direction isn't the same as where we come from and check it there's any wall block our new direction too
                            {
                                List<GameObject> newList = new List<GameObject>();
                                copyGameObjectList(pointerList, newList);

                                Color newColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f); //Just random the color for the pointer so we can see the difference when they're simulate
                                for (int i = 0; i < newList.Count; i++)
                                {
                                    newList[i].GetComponent<SpriteRenderer>().color = newColor;
                                }

                                solutionSimulator newSimulator = new solutionSimulator();
                                newSimulator.directions = new List<Static_Direction.direction>();
                                copyList(simulators.directions, newSimulator.directions);
                                newSimulator.directions.Add((Static_Direction.direction)j);

                                StartCoroutine(Check(currentPosition, currentStep + 1, (Static_Direction.direction)j, newColor, newList, newSimulator));

                            }
                        }
                    }
                    clearGameObjectList(pointerList);
                }
            }
        }
        else
        {
            clearGameObjectList(pointerList);
        }

        yield return null;
    }

    bool CompareList<ListType>(List<ListType> ListA, List<ListType> ListB)
    {
        if(ListA == null || ListB == null || ListA.Count != ListB.Count)
        {
            return false;
        }

        if(ListA.Count == 0)
        {
            return true;
        }

        for (int i = 0; i < ListA.Count; i++)
        {
            if(!ListA[i].Equals(ListB[i]))
            {
                return false;
            }
        }

        return true;
    }
    
    //Check if the Next Position in located direction is a wall
    public bool Check(Vector2 currentPosition, Static_Direction.direction direction)
    {
        Vector2 nextPosition = Static_Direction.moveTowardDirection(currentPosition, direction);
        bool checkWall = false;

        if (GameObject.Find(nextPosition.x + " x " + nextPosition.y) != null)
        {
            GameObject target = GameObject.Find(nextPosition.x + " x " + nextPosition.y);

            if (target.tag.Contains("Wall"))
            {
                checkWall = true;
            }
        }

        return checkWall;
    }

    //Check if current Position is a wall
    public bool Check(Vector2 currentPosition)
    {
        Vector2 nextPosition = currentPosition;
        bool checkWall = false;

        if (GameObject.Find(nextPosition.x + " x " + nextPosition.y) != null)
        {
            GameObject target = GameObject.Find(nextPosition.x + " x " + nextPosition.y);
            if (target.tag.Contains("Wall"))
            {
                checkWall = true;
            }
        }

        return checkWall;
    }

    //Code use to spawn pointer as a way point for the solution check
    void SpawnPointer(Vector2 gridPosition, Static_Direction.direction pointDirection, Color pointerColor, List<GameObject> pointerAddress)
    {
        GameObject pointer = Instantiate(_pointer, gridPosition, Quaternion.identity);
        float angle = 0;
        switch (pointDirection)
        {
            case Static_Direction.direction.RIGHT:
                angle = -90;
                break;
            case Static_Direction.direction.DOWN:
                angle = 180;
                break;
            case Static_Direction.direction.LEFT:
                angle = 90;
                break;
        }
        pointer.transform.Rotate(0, 0, angle);
        pointer.GetComponent<SpriteRenderer>().color = pointerColor;
        pointer.transform.parent = Stage_Manager.instance.transform;
        pointerAddress.Add(pointer);
    }

    //Doesn't need to be here
    public void copyList<TYPE>(List<TYPE> originalList, List<TYPE> newList)
    {
        for (int i = 0; i < originalList.Count; i++)
        {
            newList.Add(originalList[i]);
        }
    }
    //Doesn't need to be here
    void copyGameObjectList(List<GameObject> originalList, List<GameObject> newList)
    {
        for (int i = 0; i < originalList.Count; i++)
        {
            newList.Add(Instantiate(originalList[i], Stage_Manager.instance.transform));
        }
    }
    
    
    void clearGameObjectList(List<GameObject> gameObjectList)
    {
        try
        {
            for (int i = 0; i < gameObjectList.Count; i++)
            {
                Destroy(gameObjectList[i].gameObject);
            }
        }
        catch (System.Exception)
        {
            return;
        }
    }

    void finalizedSolution(solutionSimulator simulatorList)
    {

        _simulator.directions = new List<Static_Direction.direction>();
        _simulator.solutionStep = simulatorList.solutionStep;
        Static_Direction.direction LastDirection = Static_Direction.direction.NONE;

        for (int i = 0; i < simulatorList.directions.Count; i++)
        {
            Static_Direction.direction nextDirection = simulatorList.directions[i];

            switch (LastDirection)
            {
                case Static_Direction.direction.RIGHT:
                    switch (nextDirection)
                    {
                        case Static_Direction.direction.RIGHT:
                            _simulator.directions.Add(Static_Direction.direction.DOWN);
                            break;
                        case Static_Direction.direction.DOWN:
                            _simulator.directions.Add(Static_Direction.direction.LEFT);
                            break;
                        case Static_Direction.direction.LEFT:
                            _simulator.directions.Add(Static_Direction.direction.UP);
                            break;
                        case Static_Direction.direction.UP:
                            _simulator.directions.Add(Static_Direction.direction.RIGHT);
                            break;
                    }
                    break;
                case Static_Direction.direction.LEFT:
                    switch (nextDirection)
                    {
                        case Static_Direction.direction.RIGHT:
                            _simulator.directions.Add(Static_Direction.direction.UP);
                            break;
                        case Static_Direction.direction.DOWN:
                            _simulator.directions.Add(Static_Direction.direction.RIGHT);
                            break;
                        case Static_Direction.direction.LEFT:
                            _simulator.directions.Add(Static_Direction.direction.DOWN);
                            break;
                        case Static_Direction.direction.UP:
                            _simulator.directions.Add(Static_Direction.direction.LEFT);
                            break;
                    }
                    break;
                case Static_Direction.direction.UP:
                    switch (nextDirection)
                    {
                        case Static_Direction.direction.RIGHT:
                            _simulator.directions.Add(Static_Direction.direction.LEFT);
                            break;
                        case Static_Direction.direction.DOWN:
                            _simulator.directions.Add(Static_Direction.direction.UP);
                            break;
                        case Static_Direction.direction.LEFT:
                            _simulator.directions.Add(Static_Direction.direction.RIGHT);
                            break;
                        case Static_Direction.direction.UP:
                            _simulator.directions.Add(Static_Direction.direction.DOWN);
                            break;
                    }
                    break;
                case Static_Direction.direction.DOWN:
                case Static_Direction.direction.NONE:
                    _simulator.directions.Add(nextDirection);
                    break;
            }
            
            LastDirection = nextDirection;
        }
    }

    public void clearSolutions()
    {
        _usedMoves.Clear();
        _solutions = new solutionGuidance();
        _fewestStep = _maximumStep;
    }

    public void startSimulator()
    {
        clearGameObjectList(_solutions.pointerList);
        Stage_Manager.instance.simulatedDataCollected(_simulator.directions, _simulator.solutionStep);
    }
}