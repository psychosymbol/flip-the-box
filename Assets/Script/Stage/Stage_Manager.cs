﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Stage_Manager : MonoBehaviour
{

    public static Stage_Manager instance;

    public enum ControlType
    {
        Free,
        Mouse,
        Key,
        None
    }

    public ControlType _controlType;

    public float _swipeDelay;
    public float _swipeMinimumDistance;
    public float _flipSpeedParameter;
    public float _rotationDeadZone;

    [HideInInspector]
    public int _move = 0;
    [HideInInspector]
    public bool _rotating = false;
    [HideInInspector]
    public bool _letObjectMove = false;

    List<GameObject> _objectInStage = new List<GameObject>();

    [HideInInspector]
    public bool _simulating = false;

    //List<Stage_SolutionChecker.direction> directionList, int stepCount
    [HideInInspector]
    public List<Static_Direction.direction> _directionAnswer = new List<Static_Direction.direction>();
    [HideInInspector]
    public int _stepAnswer = 0;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    private void Update()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (this.transform.GetChild(i).tag.Contains("Object"))
                _objectInStage.Add(this.transform.GetChild(i).gameObject);
        }

        switch (_controlType)
        {
            case ControlType.Free:
                MouseControl();
                KeyboardControl();
                break;
            case ControlType.Mouse:
                MouseControl();
                break;
            case ControlType.Key:
                KeyboardControl();
                break;
            case ControlType.None:
                break;
        }

        

        if (_simulating)
        {
            printAnswer();

            StartCoroutine(simulatedAnswer(_directionAnswer, _stepAnswer));

            _simulating = false;
        }
    }

    void MouseControl()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(Swipe(Input.mousePosition));
        }
    }

    void KeyboardControl()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (!_rotating)
            {
                StartCoroutine(Flip(-1, 180));
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (!_rotating)
            {
                StartCoroutine(Flip(-1, 90));
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (!_rotating)
            {
                StartCoroutine(Flip(1, 90));
            }
        }
    }

    IEnumerator Swipe(Vector3 startPosition)
    {

        if (_rotating)
            yield break;

        bool swiping = true;
        int swipeDirection = 0;
        float timer = 0;
        Vector3 endPos = new Vector3();

        while (swiping)
        {
            if (Input.GetMouseButtonUp(0))
            {
                endPos = Input.mousePosition;
                if (Vector3.Distance(endPos, startPosition) > _swipeMinimumDistance)
                {
                    swipeDirection = (int)((endPos.x - startPosition.x) / Mathf.Abs(endPos.x - startPosition.x)); //Swipe Direction : Left = 1, Right = -1                         
                }
                swiping = false;
            }

            timer += Time.deltaTime;
            if (timer > _swipeDelay)
            {
                swiping = false;
            }

            yield return null;
        }

        StartCoroutine(Flip(swipeDirection, 90));
    }

    //This method is for rotating the camera to make it like the stage in game is rotating
    IEnumerator Flip(int direction, float targetAngle)
    {
        if (direction == 0)
        {
            yield break;
        }

        _rotating = true;
        _letObjectMove = false;
        _move++;

        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        Vector3 curEuler;

        //Prevent Error from the game state that doesn't have main camera in them
        try
        {
            curEuler = camera.transform.eulerAngles;
        }
        catch (System.Exception)
        {
            yield break;
        }

        float angle = 0;

        while (angle < targetAngle)
        {
            float difference = angle;
            if (angle > targetAngle - _rotationDeadZone)
            {
                angle = targetAngle;
            }
            else if (angle < targetAngle)
            {
                //Rotate with Lerp Method make it start faster but reach the end slower
                angle = Mathf.Lerp(angle, targetAngle, Time.deltaTime * _flipSpeedParameter);
                //Rotate with Move Towards Method make it move with the same speed
                //angle = Mathf.MoveTowards(angle, targetAngle, Time.deltaTime * _flipSpeedParameter); //Parameter must have high value (or it will be very slow)
            }

            difference = angle - difference;
            curEuler.z = curEuler.z + (difference * direction);

            camera.transform.eulerAngles = curEuler;
            yield return null;
        }

        _letObjectMove = true;
        _rotating = false;
    }

    public void simulatedDataCollected(List<Static_Direction.direction> directionList, int stepCount)
    {
        _directionAnswer = directionList;

        _stepAnswer = stepCount;

        Debug.Log("Step Answer is " + _stepAnswer);

        _letObjectMove = true;
        _simulating = true;

    }

    public IEnumerator simulatedAnswer(List<Static_Direction.direction> directionList, int stepCount)
    {
        int step = 0;
        Debug.Log("Start Simulator");
        while (step < stepCount)
        {
            if (!_rotating && _letObjectMove)
            {

                Debug.Log("Currently Simulator Step : " + (step + 1));
                int direction = 0;
                float targetAngle = 0;
                switch (directionList[step])
                {
                    case Static_Direction.direction.UP:
                    case Static_Direction.direction.DOWN:
                        direction = -1;
                        targetAngle = 180;
                        break;
                    case Static_Direction.direction.RIGHT:
                        direction = -1;
                        targetAngle = 90;
                        break;
                    case Static_Direction.direction.LEFT:
                        direction = 1;
                        targetAngle = 90;
                        break;
                }

                printAnswer(step);

                StartCoroutine(Flip(direction, targetAngle));

                step++;
            }
            yield return null;
        }
    }

    //return the downward direction of the puzzle to match the direction of the solution checker to let the object check it own below //CLEARR!!!
    public Static_Direction.direction GetDownWardDirection()
    {
        Static_Direction.direction downWard = Static_Direction.direction.NONE;
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");

        switch (Mathf.RoundToInt(camera.transform.localEulerAngles.z))
        {
            case 0 : downWard = Static_Direction.direction.DOWN;
                break;
            case 90: downWard = Static_Direction.direction.RIGHT;
                break;
            case 180: downWard = Static_Direction.direction.UP;
                break;
            case 270: downWard = Static_Direction.direction.LEFT;
                break;
        }
        return downWard;
    }

    void printAnswer()
    {
        for (int i = 0; i < _directionAnswer.Count; i++)
        {
            if (i == 0)
                Static_DebugText.instance.Debug(_directionAnswer[i] + " ");
            else
            {
                if (i + 1 == _directionAnswer.Count)
                    Static_DebugText.instance.DebugAdd(_directionAnswer[i].ToString());
                else
                    Static_DebugText.instance.DebugAdd(_directionAnswer[i] + " ");
            }
        }
    }

    //Use this to highlight the current step of the answer during simulation
    void printAnswer(int step)
    {


        for (int i = 0; i < _directionAnswer.Count; i++)
        {
            if (i == 0)
            {
                if (i == step)
                    Static_DebugText.instance.Debug("<b><color=red>" + _directionAnswer[i] + "</color></b> ");
                else
                    Static_DebugText.instance.Debug(_directionAnswer[i] + " ");
            }
            else
            {
                if (i + 1 == _directionAnswer.Count)
                {
                    if (i == step)
                        Static_DebugText.instance.DebugAdd("<b><color=red>" + _directionAnswer[i] + "</color></b>");
                    else
                        Static_DebugText.instance.DebugAdd(_directionAnswer[i].ToString());
                }
                else
                {
                    if (i == step)
                        Static_DebugText.instance.DebugAdd("<b><color=red>" + _directionAnswer[i] + "</color></b> ");
                    else
                        Static_DebugText.instance.DebugAdd(_directionAnswer[i] + " ");
                }
            }
        }
    }

    //Clear Object in Stage Manager
    public void ClearAllInStage()
    {
        transform.eulerAngles = Vector3.zero;
        for (int i = 0; i < this.transform.childCount; i++)
        {
            Destroy(this.transform.GetChild(i).gameObject);
        }

        //Static_DebugText.Debug("Clear all object in stage");
    }
}
