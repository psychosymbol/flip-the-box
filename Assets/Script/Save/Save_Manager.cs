﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Save_Manager : MonoBehaviour {

    public static Save_Manager instance;

    private string _pathFolder = ""; //This will be column x row from now on 'cause we want to save same type of puzzle to the same file. 

    private string _fileName = "";

    public Save_SaveStruct _currentDataSet;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);
    }

    /// <summary>
    /// Save/Load Step
    /// </summary>
    /// Save
    /// Load the save file we want to save to and check if there's an ID we want to save
    /// - in case there is, save to replace the data in that
    /// - in case there isn't, add the new ID to the save List
    /// Load
    /// Go through all the list in save to check for an ID
    /// - if match, load the save
    /// - if not, return with error message
    /// 
    /// 
    /// Change how you instantiate to >> change name and sprite of already exist grid
    /// 
    /// <returns></returns>

    private Save_SaveData CreateSaveData()
    {
        Grid_Generator gridGenerator = Grid_Generator.instance;
        //try to check if there's already a file save, if no, create new file
        _pathFolder = gridGenerator._column + "x" + gridGenerator._row;
        Save_SaveData save; //SaveData is a json file that contain a savestruct list of the puzzle
        try
        {
            save = JsonUtility.FromJson<Save_SaveData>(File.ReadAllText(Application.dataPath + "/PuzzleSave/" + _pathFolder + ".json")); //Load save from the exist json file
        }
        catch (System.Exception)
        {
            save = new Save_SaveData(); //create the new save file
        }

        //check if there's already the save ID in that file, if not, add new
        bool foundMatchID = false;
        int saveNumber = 0;

        for (int i = 0; i < save.Saves.Count; i++)
        {
            if(_fileName == save.Saves[i].ID)
            {
                foundMatchID = true;
                saveNumber = i;
                break;
            }
        }

        Save_SaveStruct saves; //a struct to save puzzle data

        if (!foundMatchID)
        {
            //Add new Save if there's no ID match -------------------------------------
            saves = new Save_SaveStruct();
            saves.ID = _fileName;
            saveNumber = save.Saves.Count; //Get list count before adding a new one make the index equal to the lastest file address, if you don't do this and get the count after you add new file, you must get the count - 1 in order to prevent an Out of Range Exception
            save.Saves.Add(saves);
        }

        saves = save.Saves[saveNumber];
        saves.gridIdentity = new List<int>();
        save.gridRow = gridGenerator._row;
        save.gridColumn = gridGenerator._column;
        saves.spriteSize = gridGenerator._spriteSize;
        saves.leastestMove = gridGenerator._maxStepForLevel;

        //if there's no object in scene it will assume that object is in (0, 0) position because of this line //Need Fix
        Vector2 objectPosition = Vector2.zero;

        if (GameObject.Find("Object") != null)
        {
            objectPosition = GameObject.Find("Object").GetComponent<Object_Behavior>().transform.position;
        }

        float row = (save.gridRow + 2) * saves.spriteSize;
        float column = (save.gridColumn + 2) * saves.spriteSize;

        ////////////still need a lot of polishing
        for (float i = row / 2; i > -(row / 2); i -= saves.spriteSize)
        {
            float yPos = i - saves.spriteSize / 2;
            for (float j = -(column / 2); j < column / 2; j += saves.spriteSize)
            {
                float xPos = j + saves.spriteSize / 2;

                if (GameObject.Find(xPos + " x " + yPos) != null)
                {
                    GameObject target = GameObject.Find(xPos + " x " + yPos);
                    int index = 0;
                    //Checking for the type of the grid
                    if (target.tag.Contains("Grid"))
                    {
                        if (new Vector2(xPos, yPos) == objectPosition) //check if this grid also contain Object
                            index = 4;
                        else
                            index = 0;
                    }
                    else if (target.tag.Contains("Wall"))
                    {
                        if (target.transform.localScale.x == saves.spriteSize) //Check if this wall is an outer wall or inner wall
                            index = 1;
                        else
                            index = 2;
                    }
                    else if (target.tag.Contains("Goal"))
                    {
                        index = 3;
                    }
                    saves.gridIdentity.Add(index);
                }
            }
        }

        return save;
    }

    public void SavePuzzle()
    {
        Save_SaveData save = CreateSaveData();
        Grid_Generator gridGenerator = Grid_Generator.instance;
        _pathFolder = gridGenerator._column + "x" + gridGenerator._row;
        string json = JsonUtility.ToJson(save);
        File.WriteAllText(Application.dataPath + "/PuzzleSave/" + _pathFolder + ".json", json);

        Debug.Log("Puzzle Saved");
    }

    public void LoadPuzzle()
    {
        Grid_Generator gridGenerator = Grid_Generator.instance;
        _pathFolder = gridGenerator._column + "x" + gridGenerator._row;

        Save_SaveData save = JsonUtility.FromJson<Save_SaveData>(File.ReadAllText(Application.dataPath + "/PuzzleSave/" + _pathFolder + ".json"));
        for (int i = 0; i < save.Saves.Count; i++)
        {
            if(save.Saves[i].ID == _fileName)
            {
                _currentDataSet = save.Saves[i];
            }
        }
        gridGenerator._row = save.gridRow;
        gridGenerator._column = save.gridColumn;
        gridGenerator._spriteSize = _currentDataSet.spriteSize;
        gridGenerator._maxStepForLevel = _currentDataSet.leastestMove;
        
        Debug.Log("Puzzle Loaded");
    }

    public void LoadPuzzle(string path, string id)
    {
        Save_SaveData save = JsonUtility.FromJson<Save_SaveData>(File.ReadAllText(path));
        Grid_Generator gridGenerator = Grid_Generator.instance;
        for (int i = 0; i < save.Saves.Count; i++)
        {
            if (save.Saves[i].ID == id)
            {
                _currentDataSet = save.Saves[i];
            }
        }
        gridGenerator._row = save.gridRow;
        gridGenerator._column = save.gridColumn;
        gridGenerator._spriteSize = _currentDataSet.spriteSize;
        gridGenerator._maxStepForLevel = _currentDataSet.leastestMove;

        Debug.Log("\"" + id + "\" Puzzle Loaded");
    }

    public void getFileName(string value)
    {
        _fileName = value;
    }
}
