﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Save_SaveData
{
    public int gridRow = 0;
    public int gridColumn = 0;
    public List<Save_SaveStruct> Saves = new List<Save_SaveStruct>();
}

[System.Serializable]
public class Save_SaveStruct
{
    public string ID = "";
    public float spriteSize = 0;
    public int leastestMove = 0;

    public List<int> gridIdentity = new List<int>();
}
