﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Grid_Generator : MonoBehaviour
{
    public static Grid_Generator instance;

    [Tooltip("Rows and Columns for grid spawn.")]
    public int _row, _column;

    [Tooltip("Size to scale sprite for larger grid.")]
    public float _spriteSize;

    [Tooltip("Step require to complete a level you want to generate.")]
    public int _maxStepForLevel;

    [Tooltip("Prefabs for element in level.")]
    [SerializeField]    
    GameObject _Grid, _Wall, _Object, _Goal;

    List<Vector2> _gridPos = new List<Vector2>();
    Dictionary<Vector2, GameObject> _gridList = new Dictionary<Vector2, GameObject>();
    Dictionary<Vector2, bool> _usedPath = new Dictionary<Vector2, bool>();

    //Dictionary<Vector2, GameObject> _gridList = new Dictionary<Vector2, GameObject>(); //this would work perfectly fine but we need a way to get the Vector2 before all this to identify the grid too
    
    /// <summary>
    /// Pattern Concept
    /// </summary>
    ///  Zigzag => index + + - - || - - + +
    ///  Ladder => index + - + - || - + - +
    ///  Loop   => index + + + + || - - - -

    int[] _pattern_None = new int[] { 0, 0, 0, 0 }; //Example of range and member of pattern array as which 0 = do nothing(this make the puzzle unsolveable), +1 = turn and left -1 = turn right
    int[] _pattern_Zigzag = new int[] { 1, 1, -1, -1 }; //S-Shape zig zag pattern
    int[] _pattern_Ladder = new int[] { 1, -1, 1, -1 }; //Ladder step pattern
    int[] _pattern_Loop = new int[] { 1, 1, 1, 1 }; //Loop Pattern

    int[][] _patternType;

    public TextMeshProUGUI _rowText, _columnText, _maxStepText, _sizeText;

    Transform _stageManager;

    bool _genGrid, _genPuzzle;

    Stage_SolutionChecker _stageSolutionChecker;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this);

    }

    void Start()
    {
        _stageManager = Stage_Manager.instance.transform;
        _patternType = new int[][] { _pattern_Zigzag, _pattern_Ladder, _pattern_Loop };
        _stageSolutionChecker = Stage_SolutionChecker.instance;
    }

    private void Update()
    {
        if(_genGrid)
        {
            Static_DebugText.instance.Debug("Generate new Grid");
            GenGrid(_row + 2, _column + 2, _stageManager); //row and column both're + by 2 for wall spawning
            _genGrid = false;
        }
        if (_genPuzzle)
        {
            Static_DebugText.instance.Debug("Generate Puzzle from saved data");
            _usedPath.Clear();
            int[] Pattern = _patternType[Random.Range(0, _patternType.Length)];
            StartCoroutine(puzzleGenerator(0, Vector2.zero, Static_Direction.direction.NONE, _maxStepForLevel, new List<GameObject>(), 0, Pattern, _stageManager));
            _genPuzzle = false;
        }

        _rowText.text = _row.ToString();
        _columnText.text = _column.ToString();
        _maxStepText.text = ": " + _maxStepForLevel.ToString();
        _sizeText.text = ": " + _spriteSize.ToString();
    }

    void GenGrid(float row, float column, Transform parent)
    {
        _gridPos = new List<Vector2>();
        _gridList.Clear();
        row *= _spriteSize;
        column *= _spriteSize;
        for (float i = row / 2; i > -(row / 2); i -= _spriteSize)
        {
            float yPos = i - _spriteSize / 2;
            for (float j = -(column / 2); j < column / 2; j += _spriteSize)
            {
                float xPos = j + _spriteSize / 2;
                GameObject Grid;
                if (i == -(row / 2) + _spriteSize || j == -(column / 2) || i == (row / 2) || j == (column / 2) - _spriteSize)
                {
                    Grid = Instantiate(_Wall, parent);
                }
                else
                {
                    Grid = Instantiate(_Grid, parent);
                }
                Grid.name = xPos + " x " + yPos;
                Vector2 gridPosition = new Vector2(xPos, yPos);
                Grid.transform.position = gridPosition;
                Grid.transform.localScale *= _spriteSize;
                _gridPos.Add(gridPosition);
                _gridList.Add(gridPosition, Grid);
            }
        }
    }

    public void GenGridFormSave()
    {
        int index = 0;
        float row = (_row + 2) * _spriteSize;
        float column = (_column + 2) * _spriteSize;

        for (float i = row / 2; i > -(row / 2); i-= _spriteSize)
        {
            float yPos = i - _spriteSize / 2;
            for (float j = -(column / 2); j < column / 2; j+= _spriteSize)
            {
                float xPos = j + _spriteSize / 2;
                GameObject Grid;
                Vector2 gridPosition = new Vector2(xPos, yPos);
                switch (Save_Manager.instance._currentDataSet.gridIdentity[index])
                {
                    case 1:                                          
                        Grid = Instantiate(_Wall, _stageManager);    
                        Grid.name = xPos + " x " + yPos;             
                        break;                                       
                    case 2:                                          
                        Grid = Instantiate(_Wall, _stageManager);    
                        Grid.name = xPos + " x " + yPos;         
                        break;                                       
                    case 3:                                          
                        Grid = Instantiate(_Goal, _stageManager);    
                        Grid.name = xPos + " x " + yPos;             
                        break;                                       
                    case 4:                                          
                        Grid = Instantiate(_Grid, _stageManager);    
                        Grid.name = xPos + " x " + yPos;             
                                                                     
                        GameObject Object;                           
                        Object = Instantiate(_Object, _stageManager);
                        Object.name = "Object";                      
                        Object.transform.position = gridPosition;    
                        Object.transform.localScale *= _spriteSize;  
                        break;                                       
                    default:                                         
                        Grid = Instantiate(_Grid, _stageManager);    
                        Grid.name = xPos + " x " + yPos;             
                        break;
                }

                index++;
                Grid.transform.position = gridPosition;
                Grid.transform.localScale *= _spriteSize;
            }
        }
    }



    /// <summary>
    /// Idea for Puzzle Generator
    /// </summary>
    /// 
    /// a.) get the first position and spawn object                                                                                                                              - DONE
    /// b.) the last position must be goal                                                                                                                                       - DONE
    /// c.) use the step to limit how far which puzzle will go                                                                                                                   - DONE
    /// d.) don't forget to rename them so the solution checker can find them                                                                                                    - DONE
    /// e.) remove the grid you spawn the new object over them too                                                                                                               - DONE
    /// f.) after you got a goal you must check if any wall is near the object (to make sure the object can land on this goal) if not spawn one                                  - DONE
    /// g.) Check for any path that object already move through and don't let object move in it again (waste movement + block old path)                                          - DOING (Still not working)
    /// h.) if there's already a wall in the path you check, use the wall that closest to the current position                                                                   - 
    /// i.) you can only find direction after you have the wall (you will know where you land and where you come from)                                                           - 
    /// j.) Add pattern to puzzle so the puzzle can be shave in the way that is easier to control                                                                                - 
    /// k.) goal must not be in the corner of the box with no wall to cover it or it will be to easy to solve                                                                    - 
    /// l.) the last spot before the goal must not be next to the outer wall with no wall to cover them                                                                          - 
    /// 
    /// still need a lot of condition to make this work 100%                                                                                                                     ---- FIXING ISSUE
    /// 1.) some time the error occur when you generate puzzle (index out of range)                                                                                              - FIXED (wrong variable in loop ln.270)
    /// 2.) some time spawn the wall over outer wall (unknown cause)                                                                                                             - FIXED (now they won't spawn wall over outer wall but can still use outer wall as a part of the puzzle)
    /// 3.) some time spawn the wall outside grid (unknown cause)                                                                                                                - FIXED (problem with while loop, now change from check all grid to check for wall only)
    /// 4.) can spawn wall over another wall (cause of random)                                                                                                                   - FIXED (this isn't the problem at all, they will use that wall twice which make a great puzzle too)
    /// 5.) the current condition still wrong (Look at GRID.png file in work folder on desktop)                                                                                  - FIXED but still need a lot of polishing
    /// 6.) an error will occur if they spot the wall in the direction they want to go (in this case, let them change direction)                                                 - FIXED 
    /// 7.) the object can't move as the generator think because at the end the object must have the wall below them (revert generator process)                                  - FIXED (revert process complete)
    /// 8.) if all the direction from that position are blocked by walls it will become infinite loop, go back one step                                                          - FIXED (not just go back one step but remove current position from potential spot list and random a new position to check)
    /// 9.) can spawn wall or goal on object (cause of random)                                                                                                                   - Fixing (Save & Check for used path Done but it's not working for some stupid shit reason)
    /// 10.) some time spawn the wall that can't enter (cause of random)                                                                                                         - Fixing this by give a pattern to spawn instead of randomly spawn
    /// 
    
    //Generate puzzle based on the design Pattern
    IEnumerator puzzleGenerator(int currentStep, Vector2 currentPosition, Static_Direction.direction direction, int stepNeed, List<GameObject> pathUsed, int patternIndex, int[] Pattern, Transform parent)
    {
        if (currentStep <= stepNeed)
        {
            //first step
            if (currentStep == 0)
            {
                int index = Random.Range(0, _gridPos.Count);

                while (_gridList[_gridPos[index]].gameObject.tag == "Wall")
                {
                    index = Random.Range(0, _gridList.Count);
                }

                currentPosition = _gridPos[index];
                _gridPos.RemoveAt(index);

                _usedPath.Add(currentPosition, true);

                GameObject Object = Instantiate(_Object, parent);
                Object.name = "Object";
                Object.transform.position = currentPosition;

                if (!_stageSolutionChecker.Check(currentPosition, Static_Direction.direction.DOWN))
                {
                    Vector2 wallPosition = currentPosition + Vector2.down;
                    Grid_Editor.replace(_gridList[wallPosition], "Wall");
                }
                List<Static_Direction.direction> directions = new List<Static_Direction.direction>();

                for (int i = 0; i < 3; i++)
                {
                    directions.Add((Static_Direction.direction)i);
                }

                index = Random.Range(0, directions.Count);


                while (_stageSolutionChecker.Check(currentPosition, directions[index]))
                {
                    directions.RemoveAt(index);
                    index = Random.Range(0, directions.Count);
                }

                direction = directions[index];                                                
            }                                                                                 
            //last step                                                                       
            else if (currentStep == stepNeed)                                                 
            {                                                                                 
                List<Vector2> newPosition = new List<Vector2>();                              
                                                                                              
                predictPath(currentPosition, direction, newPosition);                         
                                                                                              
                bool freeze = true;                                                           
                List<Vector2> tempPosition = new List<Vector2>();                             
                _stageSolutionChecker.copyList(newPosition, tempPosition);                    
                                                                                              
                int index = Random.Range(0, tempPosition.Count);
                //this break the loop even before they rid of all useless position -- need fix
                while (freeze) //this will loop until the grid we got is available //NEED FIXED FOR THIS PART << It loop infinitely
                {
                    if (tempPosition.Count == 0)
                    {
                        tempPosition.Clear();
                        //we only change the next direction so if the direction we currently go to is all block this will result in infinite loop so we need some condition to change the current direction too
                        //must change both current direction and next directions if the situation require
                        direction = Static_Direction.inverseDirection(direction); //current infinite while loop cause is around here, because it go into the state that can't go on but we still insist on having it inverse direction around there, it go on forever
                                                                                       //what more is that even if it inverse the direction but the tempPosition list already contain the list of one direction that's why it become infinite because it check the same list over and over again
                        predictPath(currentPosition, direction, newPosition);
                        _stageSolutionChecker.copyList(newPosition, tempPosition);
                    }
                    else
                    {
                        freeze = false;
                        //if this direction is not available for every spot (case if near outer wall), the while loop will be infinite

                        if (!_stageSolutionChecker.Check(tempPosition[index])) //index out of range again //tempPossition.Count = 0
                        {
                            bool value;
                            if (_usedPath.TryGetValue(tempPosition[index], out value) || _usedPath.TryGetValue(Static_Direction.moveTowardDirection(tempPosition[index], direction), out value)) //Somehow this still spawn wall over object start position //current temp fix is : change && to ||
                            {
                                freeze = true;
                            }
                        }
                        else
                        {
                            freeze = true;
                        }


                        if (freeze)
                        {
                            Debug.Log("Still Freezing at : " + tempPosition[index] + " in step : " + currentStep + " and need to land in direction : " + direction); //NEED FIXED FOR THIS PART <<
                            Debug.Log("Potential grid in list is : " + tempPosition.Count);
                            CreateDebugObject(tempPosition[index], Color.yellow, 0.5f);
                            tempPosition.RemoveAt(index); //if not we will rid of that grid as the potential spot and random for new grid and continue loop to check
                            index = Random.Range(0, tempPosition.Count);
                        }
                    }
                    yield return null;
                }

                currentPosition = newPosition[index];

                Grid_Editor.replace(_gridList[currentPosition], "Goal");

                if (!_stageSolutionChecker.Check(currentPosition, direction))
                {
                    Vector2 wallPosition = Static_Direction.moveTowardDirection(currentPosition, direction);
                    Grid_Editor.replace(_gridList[wallPosition], "Wall");
                }
            }
            //in between step
            else
            {
                List<Vector2> newPosition = new List<Vector2>(); //this list will contain all the potential grids

                predictPath(currentPosition, direction, newPosition);

                Static_Direction.direction directions = Static_Direction.direction.NONE; //get the direction we can move to in the next step
                
                //Add Pattern instead of random here //maybe there's something wrong with direction
                if (patternIndex < Pattern.Length)                                                  
                {                                                                                   
                    int newDirection = (int)direction + Pattern[patternIndex];                      
                    if (newDirection > 3)                                                           
                    {                                                                               
                        newDirection = 0;                                                           
                    }                                                                               
                    if (newDirection < 0)                                                           
                    {                                                                               
                        newDirection = 3;                                                           
                    }                                                                               
                                                                                                    
                    directions = (Static_Direction.direction)(newDirection);                   
                    patternIndex++;                                                                 
                                                                                                    
                    if(patternIndex >= Pattern.Length)                                              
                    {                                                                               
                        patternIndex = 0;                                                           
                                                                                                    
                        Pattern = _patternType[Random.Range(0, _patternType.Length)];               
                        //random for new pattern if the current pattern is used                     
                    }
                }
                ///There's still a chance that this loop will run in to the same path and won't be able to find any spot to continue and the loop will be infinite

                //Check if the current position can provide route for next step
                //Check if the wall for this position will blocked the used path too
                bool freeze = true; //freeze the loop for a chance to check if the grid we use is available
                Static_Direction.direction checkedDirection = directions;
                List<Vector2> tempPosition = new List<Vector2>(); //use temp list that contain potential grid to determine for the next grid objects move to(spot)
                _stageSolutionChecker.copyList(newPosition, tempPosition);
                int index = Random.Range(0, tempPosition.Count); //random for a grid to use

                while (freeze) //this will loop until the grid we got is available //NEED FIXED FOR THIS PART << It loop infinitely
                {
                    if (tempPosition.Count == 0)
                    {
                        tempPosition.Clear();
                        //we only change the next direction so if the direction we currently go to is all block this will result in infinite loop so we need some condition to change the current direction too
                        //must change both current direction and next directions if the situation require
                        direction = Static_Direction.inverseDirection(direction); //current infinite while loop cause is around here, because it go into the state that can't go on but we still insist on having it inverse direction around there, it go on forever
                                                                                       //what more is that even if it inverse the direction but the tempPosition list already contain the list of one direction that's why it become infinite because it check the same list over and over again
                        predictPath(currentPosition, direction, newPosition);
                        _stageSolutionChecker.copyList(newPosition, tempPosition);
                    }
                    else
                    {
                        freeze = false;
                        //if this direction is not available for every spot (case if near outer wall), the while loop will be infinite

                        if (!_stageSolutionChecker.Check(tempPosition[index], checkedDirection)) //check if we can move in the next step by checking if there's a wall in the direction to move in the next step
                        {
                            bool value;
                            if (_usedPath.TryGetValue(tempPosition[index], out value) || _usedPath.TryGetValue(Static_Direction.moveTowardDirection(tempPosition[index], direction), out value)) //the condition itself is fine but because we use try{}, there's some case that we can't check even when they use the path, but if we don't it will be an error as the key is not found
                                freeze = true;
                        }
                        else
                        {
                            freeze = true;
                        }


                        if (freeze)
                        {
                            if(checkedDirection == directions)
                            {
                                checkedDirection = Static_Direction.inverseDirection(checkedDirection);
                                Debug.Log("Inverse to check in direction : " + checkedDirection);
                                continue;
                            }
                            else
                            {
                                checkedDirection = Static_Direction.inverseDirection(checkedDirection);
                            }
                            Debug.Log("Still Freezing at : " + tempPosition[index] + " to move in direction " + checkedDirection + " in step : " + currentStep); //NEED FIXED FOR THIS PART <<
                            Debug.Log("Potential grid in list is : " + tempPosition.Count);
                            CreateDebugObject(tempPosition[index], Color.yellow, 0.5f);
                            tempPosition.RemoveAt(index); //if not we will rid of that grid as the potential spot and random for new grid and continue loop to check
                            index = Random.Range(0, tempPosition.Count);
                        }
                    }
                    yield return null;
                }
                
                currentPosition = tempPosition[index];
                for (int i = 0; i < newPosition.Count; i++)                        //this is kinda stupid, we just need to get an index somehow
                {                                                                  //this is kinda stupid, we just need to get an index somehow
                    if (currentPosition == newPosition[i])                         //this is kinda stupid, we just need to get an index somehow
                    {                                                              //this is kinda stupid, we just need to get an index somehow
                        index = i;                                                 //this is kinda stupid, we just need to get an index somehow
                        break;                                                     //this is kinda stupid, we just need to get an index somehow
                    }                                                              //this is kinda stupid, we just need to get an index somehow
                }
                //Add Path to used path here (index --> 0)
                for (int i = 0; i <= index; i++)
                {
                    try
                    {
                        _usedPath.Add(newPosition[i], true); //after that we will get all the grid from start point to the grid we got as a path
                    }
                    catch (System.Exception)
                    {
                    }
                }

                //spawn the wall to stop the object at the desire position
                if (!_stageSolutionChecker.Check(currentPosition, direction))
                {
                    Vector2 wallPosition = Static_Direction.moveTowardDirection(currentPosition, direction);
                    Grid_Editor.replace(_gridList[wallPosition], "Wall");
                }

                direction = directions;
            }
            currentStep++;
            StartCoroutine(puzzleGenerator(currentStep, currentPosition, direction, stepNeed, pathUsed, patternIndex, Pattern, parent));
        }
    }

    public void getRow(string value)
    {
        int.TryParse(value, out _row);
        Static_DebugText.instance.Debug("row = " + value);
    }

    public void getColumn(string value)
    {
        int.TryParse(value, out _column);
        Static_DebugText.instance.Debug("column = " + value);
    }

    public void getMaximumStep(string value)
    {
        int.TryParse(value, out _maxStepForLevel);
        Static_DebugText.instance.Debug("maximum step = " + value);
    }

    public void getSpriteSize(string value)
    {
        float.TryParse(value, out _spriteSize);
        Static_DebugText.instance.Debug("sprite size = " + value);
    }

    void predictPath(Vector2 Position, Static_Direction.direction direction, List<Vector2> PositionList)
    {
        Vector2 nextPosition = Position;
        PositionList.Clear();
        while (!_stageSolutionChecker.Check(nextPosition, direction))
        {
            nextPosition = Static_Direction.moveTowardDirection(nextPosition, direction);
            PositionList.Add(nextPosition);
        }
    }

    public void  CreateDebugObject(Vector2 position, Color color, float lifeTime)
    {
        GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Cube);
        temp.GetComponent<Renderer>().material.color = color;
        temp.transform.position = position;
        temp.name = "Pointer";
        Destroy(temp, lifeTime);
    }

    public void StartGenGrid()
    {
        _genGrid = true;
    }

    public void StartGenPuzzle()
    {
        _genPuzzle = true;
    }
}
