﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Grid_Editor : MonoBehaviour {
    public GameObject _grid, _wall, _goal;
    

    public static Dictionary<string, GameObject> _gridIdentity = new Dictionary<string, GameObject>();

    private void Awake()
    {
        _gridIdentity.Add("Grid", _grid);
        _gridIdentity.Add("Wall", _wall);
        _gridIdentity.Add("Goal", _goal);
    }

    public static void replace(GameObject targetGrid, string type)
    {
        var prefabType = PrefabUtility.GetPrefabType(_gridIdentity[type]);
        GameObject newObject;

        if (prefabType == PrefabType.Prefab)
        {
            newObject = (GameObject)PrefabUtility.InstantiatePrefab(_gridIdentity[type]);
        }
        else
        {
            newObject = Instantiate(_gridIdentity[type]);
        }

        if (newObject == null)
        {
            Debug.LogError("Error instantiating prefab");
            return;
        }

        Undo.RegisterCreatedObjectUndo(newObject, "Replace With Prefabs");
        newObject.transform.parent = targetGrid.transform.parent;
        newObject.transform.localPosition = targetGrid.transform.localPosition;
        newObject.transform.localRotation = targetGrid.transform.localRotation;
        newObject.transform.localScale = targetGrid.transform.localScale;
        newObject.transform.SetSiblingIndex(targetGrid.transform.GetSiblingIndex());
        newObject.name = targetGrid.transform.position.x + " x " + targetGrid.transform.position.y;
        Undo.DestroyObjectImmediate(targetGrid);
    }

    public static GameObject replace(Vector2 originalPos, GameObject targetGrid, string type)
    {
        var prefabType = PrefabUtility.GetPrefabType(_gridIdentity[type]);
        GameObject newObject;

        if (prefabType == PrefabType.Prefab)
        {
            newObject = (GameObject)PrefabUtility.InstantiatePrefab(_gridIdentity[type]);
        }
        else
        {
            newObject = Instantiate(_gridIdentity[type]);
        }

        if (newObject == null)
        {
            Debug.LogError("Error instantiating prefab");
            return null;
        }

        Undo.RegisterCreatedObjectUndo(newObject, "Replace With Prefabs");
        newObject.transform.parent = targetGrid.transform.parent;
        newObject.transform.localPosition = targetGrid.transform.localPosition;
        newObject.transform.localRotation = targetGrid.transform.localRotation;
        newObject.transform.localScale = targetGrid.transform.localScale;
        newObject.transform.SetSiblingIndex(targetGrid.transform.GetSiblingIndex());
        newObject.name = originalPos.x + " x " + originalPos.y;
        Undo.DestroyObjectImmediate(targetGrid);

        return newObject;
    }
}
