﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grid_EditSystem : MonoBehaviour {
    
    public enum TYPE
    {
        Grid = 0,
        Wall = 1,
        Goal = 2
    }

    GameObject _selectedObject;
    Vector2 _selectedPosition;
    TYPE _selectedObjectType;

    public TYPE _selectedType;

	void Update () {

        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector2.zero, 0.0f);

        if(Input.GetMouseButtonDown(0))
        {
            //start left mouse click
            if(hit)
            {
                _selectedPosition = hit.transform.position;
                _selectedObject = hit.transform.gameObject;
                if (_selectedObject.name.Contains("Grid"))
                {
                    _selectedObjectType = TYPE.Grid;
                }
                else if (_selectedObject.name.Contains("Wall"))
                {
                    _selectedObjectType = TYPE.Wall;
                }
                else if (_selectedObject.name.Contains("Goal"))
                {
                    _selectedObjectType = TYPE.Goal;
                }
            }
        }
        if(Input.GetMouseButton(0))
        {
            //hold left click
            if (hit)
            {
                _selectedObject.transform.position = mousePosition;
                _selectedObject.transform.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            }
        }
        else if(Input.GetMouseButtonUp(0))
        {
            //release left click
            if (_selectedObject != null)
            {
                if(hit)
                {
                    TYPE targetType = TYPE.Grid;

                    if (hit.transform.name.Contains("Grid"))
                    {
                        targetType = TYPE.Grid;
                    }
                    else if (hit.transform.name.Contains("Wall"))
                    {
                        targetType = TYPE.Wall;
                    }
                    else if (hit.transform.name.Contains("Goal"))
                    {
                        targetType = TYPE.Goal;
                    }

                    _selectedObject = Grid_Editor.replace(_selectedPosition, _selectedObject, targetType.ToString());
                    Grid_Editor.replace(hit.transform.gameObject, _selectedObjectType.ToString());
                }
                _selectedObject.transform.position = _selectedPosition;
                _selectedPosition = Vector2.zero;
                _selectedObject = null;
            }
        }
        else if(Input.GetMouseButtonDown(1))
        {
            //right mouse click
            //Change gameObject to ray hit object
            if (hit)
                Grid_Editor.replace(hit.transform.gameObject, _selectedType.ToString());
        }

	}

    public void getType(int value)
    {
        _selectedType = (TYPE)value;
    }
}
